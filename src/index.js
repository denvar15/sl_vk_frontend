import "core-js/features/map";
import "core-js/features/set";
import React from 'react';
import ReactDOM from 'react-dom';
import App1 from './App1';
import { RouterProvider } from 'react-router5'
import createRouter from './create-router'
import bridge from "@vkontakte/vk-bridge";

bridge.send("VKWebAppInit");

const router = createRouter()

router.start(() => {
    ReactDOM.render((
        <RouterProvider router={router}>
            <App1/>
        </RouterProvider>
    ), document.getElementById('root'))
})
/*
if (process.env.NODE_ENV === "development") {
    import("./eruda").then(({ default: eruda }) => {}); //runtime download
}
 */
