import React from 'react'
import { PanelHeader, FormLayout, Textarea, Input, FixedLayout, Button, Div, platform, ANDROID, FormStatus } from '@vkontakte/vkui'
import '@vkontakte/vkui/dist/vkui.css'
import PanelHeaderBack from '@vkontakte/vkui/dist/components/PanelHeaderBack/PanelHeaderBack'
import Icon24Done from '@vkontakte/icons/dist/24/done'

class AddTask extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			name : '',
			text : '',
			error : false,
			popout: null,
			tags: ''
		};
	}

	onClickAddTask = () => {
		let {
			addTask,
			router
		} = this.props

		let {
			name,
			text,
			tags
		} = this.state

		if (name !== '' && text !== '') {
			this.setState({ error : false })
			addTask({
				name, text, tags
			})
			//router.navigateToDefault()
		} else {
			this.setState({ error : true })
		}

	}

	onChangeNameTask = (e) => {
		const name = e.target.value
		this.setState({ name })
	}

	onChangeTextTask = (e) => {
		const text = e.target.value
		this.setState({ text })
	}

	onChangeTagsTask = (e) => {
		const tags = e.target.value
		this.setState({tags})
	}

	render() {

		let {
			router
		} = this.props

		const osname = platform()

		return (
			<div>
                <PanelHeader
					left={
						<PanelHeaderBack
							onClick={()=>router.navigate('tasks')}
						/>
					}
				>
                Добавление
                </PanelHeader>
                <FormLayout>
					{
						this.state.error === true &&
						<FormStatus title="Некорректные поля" state="error">
							Заполните все поля
						</FormStatus>
					}
					<Input
						style={{background:'linear-gradient(90deg, rgba(0,147,134,0.1) 0%, rgba(27,187,134,0.1) 56%, ' +
								'rgba(243,255,142,0.1) 100%)'}}
						onChange={this.onChangeNameTask}
						type='text'
						value={this.state.name}
						placeholder='Напишите текст вопроса'
					/>
					<div className='FormField'>
						<textarea
							className='Textarea'
							onChange={this.onChangeTextTask}
							value={this.state.text}
							placeholder='Напишите текст ответа'
						/>
					</div>
					<div className='FormField'>
						<textarea
							className='Textarea'
							onChange={this.onChangeTagsTask}
							value={this.state.tags}
							placeholder='Напишите ваши теги через пробел'
						/>
					</div>
				</FormLayout>
				<FixedLayout vertical='bottom'>
					{
						osname === ANDROID ?
						<Div style={{ float : 'right' }}>
							<Button
								style={{backgroundColor:'#0762D9'}}
								className='FixedBottomButton'
								onClick={(e) => this.onClickAddTask(e)}
							>
								<Icon24Done/>
							</Button>
						</Div>
						:
						<Div>
							<Button
								style={{backgroundColor:'#0762D9'}}
								size='xl'
								onClick={(e) => this.onClickAddTask(e)}
							>
								Добавить
							</Button>
						</Div>
					}
				</FixedLayout>
            </div>
		);
	}
}

export default AddTask;
