import React from 'react'
import { PanelHeader, FormLayout, Textarea, Input, FixedLayout, Button, Div, platform, ANDROID, FormStatus } from '@vkontakte/vkui'
import '@vkontakte/vkui/dist/vkui.css'
import PanelHeaderBack from '@vkontakte/vkui/dist/components/PanelHeaderBack/PanelHeaderBack'
import Icon24Done from '@vkontakte/icons/dist/24/done'
import axios from "axios";

class EditTask extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			slug : null,
			title : '',
			body : '',
			tagList : '',
			error : false
		};
	}

	componentDidMount() {
		let {
			task
		} = this.props

		this.setState({ ...task })
	}

	onClickEditTask = () => {
		let {
			editTask,
			router
		} = this.props

		let {
			slug,
			title,
			body,
			tagList
		} = this.state

		if (title !== '' && body !== '') {
			this.setState({ error : false })
			editTask({
				slug, title, body, tagList
			})
			router.navigateToDefault()
        } else {
			this.setState({ error : true })
		}

	}

	onChangeNameTask = (e) => {
		const title = e.target.value
		this.setState({ title })
	}

	onChangeTextTask = (e) => {
		const body = e.target.value
		this.setState({ body })
	}

	onChangeTagsTask = (e) => {
		const tagList = e.target.value
		this.setState({tagList})
	}


	render() {

		let {
			task,
			router
		} = this.props

		const osname = platform()

		return (
			<div>
				{
					typeof task !== 'undefined' &&
					<div>
						<PanelHeader
							left={
								<PanelHeaderBack
									onClick={()=>router.navigate('task', { slug : task.slug })}
								/>
							}
						>
						Редактирование
						</PanelHeader>
						<FormLayout>
							{
								this.state.error === true &&
								<FormStatus title="Некорректные поля" state="error">
									Заполните все поля
								</FormStatus>
							}
							<Input
								style={{background:'linear-gradient(90deg, rgba(0,147,134,0.1) 0%, rgba(27,187,134,0.1) ' +
										'56%, rgba(243,255,142,0.1) 100%)'}}
								onChange={this.onChangeNameTask}
								type='text'
								value={this.state.title}
								placeholder='Напишите текст вопроса'
							/>
							<div className='FormField'>
								<textarea
									className='Textarea'
									onChange={this.onChangeTextTask}
									value={this.state.body}
									placeholder='Напишите текст ответа'
								/>
							</div>
							<div className='FormField'>
								<textarea
									className='Textarea'
									onChange={this.onChangeTagsTask}
									value={this.state.tagList}
									placeholder='Напишите ваши теги через пробел'
								/>
							</div>
						</FormLayout>
						<FixedLayout vertical='bottom'>
							{
								osname === ANDROID ?
								<Div style={{ float : 'right' }}>
									<Button
										className='FixedBottomButton'
										onClick={() => this.onClickEditTask()}
									>
										<Icon24Done/>
									</Button>
								</Div>
								:
								<Div>
									<Button
										size='xl'
										onClick={() => this.onClickEditTask()}
									>
										Сохранить
									</Button>
								</Div>
							}
						</FixedLayout>
					</div>
				}
            </div>
		);
	}
}

export default EditTask;
