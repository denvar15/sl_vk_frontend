import React from 'react'
import {List, Cell, PanelHeader, platform, ANDROID, PanelHeaderButton, Counter, Text, Alert} from '@vkontakte/vkui'
import '@vkontakte/vkui/dist/vkui.css'
import Icon24Pin from '@vkontakte/icons/dist/24/pin';

class Tasks extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            popout: null,
        }
    }

    counterFunc = (task) => {
        const divStyle = {
            color: 'white',
            backgroundColor: `rgb(${0},${240 - task.learningIndicator * 30},${10 + task.learningIndicator * 30})`,
        };
        return <Counter style={divStyle}>{task.learningIndicator}</Counter>
    }

    tasksRender = (tasks, props) => {
        const osname = platform()
        if (tasks.length > 0) {
            return (
                <List style={{ paddingTop : (osname === ANDROID) ? 56 : 48 }}>
                {
                    tasks.map((task, index) => (
                        <Cell
                            multiline
                            expandable
                            removable={props.removable}
                            selectable={props.selectable}
                            key={task.slug}
                            id={task.slug}
                            onRemove={() => props.deleteTask(task.slug)}
                            onClick={()=> {
                                if (!props.selectable) {
                                    props.setCurrentTaskId(task.slug)
                                    props.router.navigate('task', { slug : task.slug })
                                } else {
                                    props.favoriteTask(task.slug)
                                }
                            }
                            }
                            indicator={this.counterFunc(task)}
                        >
                            <Text weight={task.favorite ? 'semibold': 'regular'} style={{maxWidth:'90vw',
                                color: task.favorite ? '#0762D9':'black'}}> {task.title}</Text>
                        </Cell>
                    ))
                }
                </List>
            )
        } else {
            if (props.reolad !== 1) {
                props.nothingAlert();
            }
        }
    }

	render() {

        let {
            tasks,
            router,
            setCurrentTaskId,
            setSelectable,
            deleteTask,
            favoriteTask,
            removable,
            selectable
        } = this.props

        const osname = platform()

        return (
			<div style={{marginBottom:'50px', marginTop:`${osname === ANDROID ? '0px': '10px'}`}}>
                <PanelHeader
                    left={
                        <PanelHeaderButton
                            onClick={() => setSelectable()}
                        >
                            <Icon24Pin style={{color:'#0762D9'}}/>
                        </PanelHeaderButton >
                    }
                >
                    Вопросы
                </PanelHeader>
                {this.tasksRender(tasks, this.props)}
            </div>
		);
	}
}

export default Tasks;
