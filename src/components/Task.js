import React from 'react'
import { PanelHeader, Title, Div, Card, Group, Button, Headline } from '@vkontakte/vkui'
import PanelHeaderBack from '@vkontakte/vkui/dist/components/PanelHeaderBack/PanelHeaderBack'

function Task(props) {

	const task = props.task
	const editTask = props.editTask
	const router = props.router

	function visibility(task) {
		let desc = document.getElementById(task.slug);
		desc.style.visibility = desc.style.visibility === 'visible' ? 'hidden': 'visible';
	}

	function redirectAndIndex(task, ind) {
		task.learningIndicator += ind;
		task.timesWasAsked += 1;
		editTask(task)
	}

	let re = /,/gi;

	return (
		<div>
           <PanelHeader
				left={
					<PanelHeaderBack
						onClick={()=>router.navigate('tasks', {reload: true})}
					/>
				}
			>
            Вопрос
            </PanelHeader>
			{
				typeof task !== 'undefined' &&
				<Group style={{marginTop: '45%', marginBottom: '30%'}}>
					<Card size="xl" mode="shadow"
						  style={{
							  display: 'flex', justifyContent: 'center', alignItems: 'center', minHeight: '250px',
							  textAlign: 'center'
						  }}
						  onClick={visibility.bind(this, task)}
					>
						<Title style={{wordWrap: 'break-word', margin: '10% 10% 0 10%', maxWidth: '90vw'}} level="1"
							   weight="bold">{task.title}</Title>
						<Div id={task.slug} style={{visibility: "hidden"}}>
							<Div style={{wordWrap: 'break-word', maxWidth: '90vw'}}>
								{task.body}
							</Div>
							<Div>
								<Button style={{backgroundColor: '#044BD9'}}
										onClick={redirectAndIndex.bind(this, task, 1)}>Не верно</Button>
								<Button style={{margin: '10px', backgroundColor: '#04D976'}}
										onClick={redirectAndIndex.bind(this, task, -1)}>Верно</Button>
							</Div>
						</Div>
					</Card>
					<Headline weight='regular' style={{textAlign:'center', margin: '10% 20% 0 20%', color:'grey'}}>
						{task.tagList.toString().replace(re, ' ')}
					</Headline>
				</Group>
			}
        </div>
	)
}

export default Task
