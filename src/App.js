import React from 'react'
import {View, Panel, FixedLayout, Div, Button, platform, ANDROID, Root, Alert, Search, Input} from '@vkontakte/vkui'
import '@vkontakte/vkui/dist/vkui.css'
import Tasks from './components/Tasks'
import AddTask from './components/AddTask'
import Task from './components/Task'
import EditTask from './components/EditTask'
import Icon24Add from '@vkontakte/icons/dist/24/add'
import Icon24Write from '@vkontakte/icons/dist/24/write'
import { RouteNode } from 'react-router5'
import Icon24Delete from '@vkontakte/icons/dist/24/delete'
import './custom.css'
import bridge from '@vkontakte/vk-bridge';
import axios from 'axios'
import ScreenSpinner from '@vkontakte/vkui/dist/components/ScreenSpinner/ScreenSpinner';

class App extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			tasks : [
				{
					slug : 1,
					title : 'Идёт загрузка',
					body : 'Ждите'
				},
			],
			currentTaskId : null,
			search : '',
			removable : false,
			favoritable : false,
			popout: null,
			posts:[
				{
					slug : 1,
					title : 'Идёт загрузка',
					body : 'Ждите'
				},
			],
		}
	}

	componentDidMount() {
		/*
		this.user().then(() => {
			this.state.tasks.map((task) => {task.name = this.state.user.first_name})
		})
		 */
	}

	onChangeSearch = (e) => {
		this.setState({ search: e.target.value })
	}

	addTask = (task) => {
		this.setState({popout:null})
		this.setState({popout:<ScreenSpinner size='large' />})
		const taskBack = {};
		taskBack.title = task.name;
		taskBack.body = task.text;
		taskBack.description = task.text;
		taskBack.tagList = task.tags.split(' ');
		axios.post(this.props.url + '/api/articles', {
			id: this.userVK.id,
			article: taskBack,
		}).then((res) => {
			axios.get(this.props.url + '/api/articles', {
				params: {
					id: this.userVK.id,
					limit: 10,
				}
			}).then((posts) => {
				axios.post(this.props.url + '/api/users/push', {
					id: this.userVK.uid,
					message: taskBack.title,
					token: this.userVK.token
				})
				this.setState({popout:null})
				this.setState({posts: posts.data.articles})
				//this.props.router.navigate('task', { slug : res.data.article.slug })
				this.props.router.navigate('tasks')
				this.Notifications();
			})
		})
	}

	favoriteTask = (slug) => {
		this.setState({popout:<ScreenSpinner size='large' />})
		let newTask = this.state.posts.find((task) => {if (task.slug === slug) {return task}})
		newTask.favorite = !newTask.favorite;
		axios.put(this.props.url + '/api/articles/' + newTask.slug, {
			article: newTask,
			id: this.userVK.id
		}).then(() => {
			axios.get(this.props.url + '/api/articles', {
				params: {
					id: this.userVK.id,
					limit: 10,
				}
			}).then((posts) => {
				this.setState({posts: posts.data.articles})
				this.setState({popout:null})
			})
		})
	}

	deleteTask = (slug) => {
		this.setState({popout:<ScreenSpinner size='large' />})
		let newTask = this.state.posts.find((task) => {if (task.slug === slug) {return task}})
		axios.delete(this.props.url + '/api/articles/' + newTask.slug,
			{ data: { id: this.userVK.id }
		}).then(() => {
			axios.get(this.props.url + '/api/articles', {
				params: {
					id: this.userVK.id,
					limit: 10,
				}
			}).then((posts) => {
				this.setState({posts: posts.data.articles})
				this.setState({popout:null})
			})
		})
	}

	onFavoritingPosts = (task) => {
		this.setState({ favoritable : !this.state.favoritable });
	}

	onRemovableTasks = () => this.setState({ removable : !this.state.removable })

	setCurrentTaskId = (currentTaskId) => this.setState({ currentTaskId })

	editTask = (newTask) => {
		this.setState({popout:<ScreenSpinner size='large' />})
		axios.put(this.props.url + '/api/articles/' + newTask.slug, {
			article: newTask,
			id: this.userVK.id
		}).then(() => {
			axios.get(this.props.url + '/api/articles', {
				params: {
					id: this.userVK.id,
					limit: 10,
				}
			}).then((posts) => {
				try {
					this.props.router.navigate('task', { slug : this.nextSlug(newTask) })
                    this.setState({posts: posts.data.articles})
                } catch(e) {
                    this.setState({posts: posts.data.articles})
                    this.props.router.navigate('tasks')
				}
				this.setState({popout:null})
			})
		})
	}

	get tasks () {
		const search = this.state.search.toLowerCase()
		if (search !== '') {this.reoladAlert = 1}
		return this.state.posts.filter((task) =>
			task.title.toLowerCase().indexOf(search) > -1 ||
			task.tagList.toString().toLowerCase().indexOf(search) > -1)
	}

	get task () {
		const id = this.props.route.params.slug|| this.state.currentTaskId
		return this.state.posts.filter((task) =>
			task.slug === id
		)
	}

	nextSlug = (task) => {
		let i = 0;
		while (this.state.posts[i].slug !== task.slug) {
			i += 1
		}
		return this.state.posts[i+1].slug
	}

	async user() {
		let user = await bridge.send('VKWebAppGetUserInfo');
		this.setState({user:user})
	}

	async Notifications() {
		try {
			let notifications = await bridge.send("VKWebAppAllowNotifications");
		} catch(err) {
			this.setState({popout:
					<Alert
						actions={[{
							title: 'Закрыть',
							mode: 'cancel',
							action: () => {
								this.setState({popout: null})
							},
						}, {
							title: 'Открыть',
							action: () => {
								bridge.send("VKWebAppAllowNotifications")
							},
						}]}
					>
						<h2>Пожалуйста, разрешите уведомления</h2>
						<p>Откройте окно выбора и нажмите разрешить</p>
					</Alert>
			});
		}
	}

	nothingAlert = () => {
		this.reoladAlert = 1;
		this.setState({
			popout:
				<Alert
					actions={[{
						title: 'Закрыть',
						mode: 'cancel',
						action: () => {
							this.setState({popout: null})
						},
					}]}
				>
					<h2>Здесь пока ничего нет. Самое время добавить вопросы!</h2>
					<p>Нажмите на знак плюса в правом нижнем углу экрана</p>
				</Alert>
		});
	}


	render() {
		let {
			route,
			router,
			user,
			posts
		} = this.props

		const osname = platform()
		//const activeView = (route.name === 'add') ? 'addView' : 'tasksView'
		const activeView = 'tasksView'
		const activePanel = route.name

		if (user) {
			this.userVK = user;
		}
		if (posts !== null) {
			if (this.reolad !== 1) {
				this.setState({posts:posts});
				this.reolad = 1;
			}
		}

		return (
			<Root activeView={activeView}>
				<View activePanel={activePanel} id='tasksView' popout={this.state.popout} >
					<Panel id='tasks'>
						<FixedLayout vertical='top' style={{paddingTop:'65px', backgroundColor:'white'}}>
							<Search style={{background:'linear-gradient(90deg, rgba(0,147,134,0.1) 0%, ' +
									'rgba(27,187,134,0.1) 56%, rgba(243,255,142,0.1) 100%)'}}
									value={this.state.search} onChange={this.onChangeSearch} after={null}/>
						</FixedLayout>
						<Tasks
							router={router}
							tasks={this.tasks}
							removable={this.state.removable}
							selectable={this.state.favoritable}
							setSelectable={this.onFavoritingPosts}
							setCurrentTaskId={this.setCurrentTaskId}
							deleteTask={this.deleteTask}
							favoriteTask={this.favoriteTask}
							nothingAlert={this.nothingAlert}
							reolad={this.reoladAlert}
						/>
						<FixedLayout vertical='bottom'>
							{
								osname === ANDROID ?
								<Div style={{ float : 'right' }}>
									<Button
                                        style={{backgroundColor:'#0762D9'}}
										className='FixedBottomButton'
										onClick={()=>{router.navigate('add')}}
									>
										<Icon24Add/>
									</Button>
								</Div>
								:
								<Div>
									<Button
										size="xl"
										onClick={()=>router.navigate('add')}
									>
										Новая задача
									</Button>
								</Div>
							}
							{
								osname === ANDROID ?
								<Div>
									<Button
                                        style={{backgroundColor:'#0762D9'}}
										className='FixedBottomButton'
										onClick={() => this.onRemovableTasks()}
									>
										<Icon24Delete/>
									</Button>
								</Div>
								:
								false
							}
						</FixedLayout>
					</Panel>

					<Panel id='task'>
						<Task
							router={router}
							task={this.task[0]}
                            editTask={this.editTask}
						/>
						<FixedLayout vertical='bottom'>
							{
								osname === ANDROID ?
								<Div style={{ float : 'right' }}>
									<Button
                                        style={{backgroundColor:'#0762D9'}}
                                        className='FixedBottomButton'
										onClick={()=>router.navigate('edit', { slug : this.task[0].slug })}
									>
										<Icon24Write/>
									</Button>
								</Div>
								:
								<Div>
									<Button
										size="xl"
										onClick={()=>router.navigate('edit', { slug : this.task[0].slug })}
									>
										Редактировать
									</Button>
								</Div>
							}
						</FixedLayout>
					</Panel>

					<Panel id="edit" theme="white">
						<EditTask
							router={router}
							task={this.task[0]}
							editTask={this.editTask}
						/>
					</Panel>
					<Panel id='add' theme="white">
						<AddTask
							router={router}
							addTask={this.addTask}
						/>
					</Panel>
				</View>
				<View activePanel={activePanel} id='addView'>

				</View>
			</Root>
		)
	}
}

export default (props) => (
    <RouteNode nodeName="">
        {({ route }) => <App route={route} {...props}/>}
    </RouteNode>
)
