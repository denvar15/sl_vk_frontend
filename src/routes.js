export default [
    { name: 'tasks', path: '/tasks' },
    { name: 'task', path: '/task/:slug' },
    { name: 'edit', path: '/edit/:slug' },
    { name: 'add', path: '/add' }
]
