import React, { useState, useEffect } from 'react';
import bridge from '@vkontakte/vk-bridge';
import View from '@vkontakte/vkui/dist/components/View/View';
import ScreenSpinner from '@vkontakte/vkui/dist/components/ScreenSpinner/ScreenSpinner';
import '@vkontakte/vkui/dist/vkui.css';
import App from './App'
import {useRoute} from "react-router5";
import axios from "axios";

const App1 = () => {
    const [activePanel, setActivePanel] = useState('example');
    const [fetchedUser, setUser] = useState(null);
    const [fetchedToken, setToken] = useState(null);
    const [fetchedPosts, setPosts] = useState(null);
    const [popout, setPopout] = useState(<ScreenSpinner size='large' />);
    const { router } = useRoute();
    const url = 'https://vkminiback.herokuapp.com';

    useEffect(() => {
        bridge.subscribe(({ detail: { type, data }}) => {
            if (type === 'VKWebAppUpdateConfig') {
                const schemeAttribute = document.createAttribute('scheme');
                schemeAttribute.value = 'client_light';
                document.body.attributes.setNamedItem(schemeAttribute);
            }
        });
        async function fetchData() {
            const user = await bridge.send('VKWebAppGetUserInfo');
            const token = await bridge.send("VKWebAppGetAuthToken", {"app_id": 7657902, "scope": ""});
            setToken(token);
            const id = await axios.post(url + '/api/users/login', {
                token: token,
                user: user,
            })
            if (id) {
                user.token = token;
                user.uid = user.id;
                user.id = id.data.user;
                setUser(user);
                const posts = await axios.get(url + '/api/articles', {
                    params: {
                        id: id.data.user,
                        limit: 10,
                    }
                })
                setPosts(posts.data.articles);
            } else {
                setPopout(<ScreenSpinner size='large' />);
            }
            setPopout(null);
        }
        fetchData();
    }, []);

    const go = e => {
        setActivePanel(e.currentTarget.dataset.to);
    };

    const closePopout = () => {
        setPopout(null);
    }

    return (
        <View activePanel={activePanel} popout={popout}>
            <App id='example' router={router} user={fetchedUser} posts={fetchedPosts} url={url}/>
        </View>
    );
}

export default App1;

